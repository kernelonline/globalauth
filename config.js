"use strict";
const secret = process.env.JWT_SECRET || 'secret';
const config = {
    secret: secret
};
let serveruri = 'http://localhost:3000';
if (process.env.SERVERURI)
    serveruri = process.env.SERVERURI;
if (process.env.TWITTER_SECRET && process.env.TWITTER_ID) {
    config.twitter = {
        consumerKey: process.env.TWITTER_ID,
        consumerSecret: process.env.TWITTER_SECRET,
        callbackURL: serveruri + '/auth/twitter/callback'
    };
}
if (process.env.GITHUB_SECRET && process.env.GITHUB_ID) {
    config.github = {
        clientID: process.env.GITHUB_ID,
        clientSecret: process.env.GITHUB_SECRET,
        callbackURL: serveruri + '/auth/github/callback'
    };
}
if (process.env.INSTAGRAM_SECRET && process.env.INSTAGRAM_ID) {
    config.instagram = {
        clientID: process.env.INSTAGRAM_ID,
        clientSecret: process.env.INSTAGRAM_SECRET,
        callbackURL: serveruri + '/auth/instagram/callback'
    };
}
if (process.env.FACEBOOK_SECRET && process.env.FACEBOOK_ID) {
    config.facebook = {
        clientID: process.env.FACEBOOK_ID,
        clientSecret: process.env.FACEBOOK_SECRET,
        callbackURL: serveruri + '/auth/facebook/callback'
    };
}
if (process.env.GOOGLE_SECRET && process.env.GOOGLE_ID) {
    config.google = {
        clientID: process.env.GOOGLE_ID,
        clientSecret: process.env.GOOGLE_SECRET,
        callbackURL: serveruri + '/auth/google/callback'
    };
}
module.exports = config;
//# sourceMappingURL=config.js.map