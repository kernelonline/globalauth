export interface IUser {
    passport_id: string
    email: string
    password?: string
    provider: string
    provider_uid: string
    firstName: string
    lastName: string
    gender: string
    avatar: string
}
export interface IUserLogin {
    passport_id: string
    email: string
    token:string
    provider: string
    provider_uid: string
    firstName: string
    lastName: string
    gender: string
    avatar: string
}


export interface IFBAnswer {
    
}


export interface IGoogleAnswer {
    
}