$.fn.serializeObject = function() {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name]) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};


function register() {

    var data = $("#loginform").serializeObject()



    $.post('/auth/local/register', data, function(a) {

        console.log(a)


    })

}

function login() {

    var data = $("#loginform").serializeObject()



    $.post('/auth/local/login', data, function(a) {

        console.log(a)


    })

}