"use strict";
const passport = require("passport");
const User = require("../models/user");
const init = require("./init");
const TwitterStrategy = require('passport-twitter').Strategy;
const config = require("../../config");
if (config.twitter) {
    passport.use(new TwitterStrategy({
        consumerKey: config.twitter.consumerKey,
        consumerSecret: config.twitter.consumerSecret,
        callbackURL: config.twitter.callbackURL
    }, function (accessToken, refreshToken, profile, done) {
        var searchQuery = {
            name: profile.displayName,
            provider: 'twitter'
        };
        var updates = {
            name: profile.displayName,
            someID: profile.id
        };
        var options = {
            upsert: true,
            new: true
        };
        User.findOneAndUpdate(searchQuery, updates, options, function (err, user) {
            if (err) {
                return done(err);
            }
            else {
                return done(null, user);
            }
        });
    }));
}
init();
module.exports = passport;
//# sourceMappingURL=twitter.js.map