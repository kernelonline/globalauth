"use strict";
const passport = require("passport");
const unicoid_1 = require("unicoid");
const axios_1 = require("axios");
const User = require("../models/user");
const init = require("./init");
const FacebookStrategy = require('passport-facebook').Strategy;
console.log("new");
const config = require("../../config");
if (config.facebook) {
    passport.use(new FacebookStrategy({
        clientID: config.facebook.clientID,
        clientSecret: config.facebook.clientSecret,
        callbackURL: config.facebook.callbackURL,
        profileFields: ['email', 'id', 'gender', 'first_name', 'last_name']
    }, function (accessToken, refreshToken, profile, done) {
        const searchQuery = {
            email: profile.emails[0].value,
            provider: 'facebook'
        };
        console.log('PROFILE FB', searchQuery, profile);
        const options = {
            upsert: true,
            new: true
        };
        User.find(searchQuery, function (err, user) {
            if (err) {
                console.log(err);
                return done(err);
            }
            else if (user.length !== 0) {
                console.log(user);
                console.log('login');
                const u = {
                    email: profile._json.email,
                    passport_id: user[0].passport_id,
                    provider: 'facebook',
                    provider_uid: profile._json.id,
                    firstName: profile._json.first_name,
                    lastName: profile._json.last_name,
                    gender: profile._json.gender,
                    avatar: 'https://graph.facebook.com/' + profile._json.id + '/picture?type=large'
                };
                User.findOneAndUpdate(searchQuery, u, options, function (err, user) {
                    if (err) {
                        return done(err);
                    }
                    else {
                        return done(null, user);
                    }
                });
            }
            else if (process.env.REGISTER) {
                console.log(user);
                console.log('register');
                const u = {
                    email: profile._json.email,
                    passport_id: 'u_' + unicoid_1.uniqueid(5) + '_' + Date.now(),
                    provider: 'facebook',
                    provider_uid: profile._json.id,
                    firstName: profile._json.first_name,
                    lastName: profile._json.last_name,
                    gender: profile._json.gender,
                    avatar: 'https://graph.facebook.com/' + profile._json.id + '/picture?type=large'
                };
                User.findOneAndUpdate(searchQuery, u, options, function (err, user) {
                    if (err) {
                        return done(err);
                    }
                    else {
                        if (process.env.REGISTERHOOK)
                            axios_1.default.post(process.env.REGISTERHOOK, u);
                        return done(null, user);
                    }
                });
            }
            else {
                return done(new Error('authorization not allowed'));
            }
        });
    }));
}
init();
module.exports = passport;
//# sourceMappingURL=facebook.js.map