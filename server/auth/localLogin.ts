import * as passport from "passport"
import * as _ from 'lodash'

import * as User from '../models/user'
import * as init from './init'
import * as localStrategy from 'passport-local'

const LocalStrategy = localStrategy.Strategy



passport.use(new LocalStrategy({
  usernameField: 'email',
  passwordField: 'passwd'
},
  function (email, passwd, done) {

    const searchQuery = {
      provider: 'local',
      email: email,
      password: passwd
    }


    // update the user if s/he exists or add a new user
    User.find(searchQuery, function (err, user: any) {
      if (err) {
        console.log("error:" + err)
        return done(err)
      } else if (user.length === 1) {
        const u: any = {}
        for (let i = 0; i < Object.keys(user[0]).length; i++) {
          if (Object.keys(user[0])[i] !== 'password') {
            u[Object.keys(user[0])[i]] = user[0][Object.keys(user[0])[i]]
          }
        }
        return done(null, u)
      } else {
        return done("unauthorized")
      }
    })
  }

))

// serialize user into the session
init()


export = passport
