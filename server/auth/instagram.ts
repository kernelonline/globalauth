import * as passport from "passport"

import * as User from '../models/user'
import * as init from './init'

const InstagramStrategy = require('passport-instagram').Strategy

import * as config from '../../config'

if (config.instagram) {

  passport.use(new InstagramStrategy({
    clientID: config.instagram.clientID,
    clientSecret: config.instagram.clientSecret,
    callbackURL: config.instagram.callbackURL,
    profileFields: ['id', 'emails', 'name']
  },
    function (accessToken, refreshToken, profile, done) {
      console.log(accessToken, refreshToken, profile)

      var searchQuery = {
        passport_id: profile.id,
        provider: 'instagram'
      }

      var updates = {
        name: '',
        passport_id: profile.id,
        provider: 'instagram',
        email: ''
      }


      var options = {
        upsert: true,
        new:true
      }

      // update the user if s/he exists or add a new user
      User.findOneAndUpdate(searchQuery, updates, options, function (err, user) {
        if (err) {
          return done(err)
        } else {
          return done(null, user)
        }
      })
    }

  ))
}
// serialize user into the session
init()


export = passport
