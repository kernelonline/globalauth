"use strict";
const passport = require("passport");
const User = require("../models/user");
const init = require("./init");
const GitHubStrategy = require('passport-github2').Strategy;
const config = require("../../config");
if (config.github) {
    passport.use(new GitHubStrategy({
        clientID: config.github.clientID,
        clientSecret: config.github.clientSecret,
        callbackURL: config.github.callbackURL
    }, function (accessToken, refreshToken, profile, done) {
        var searchQuery = {
            name: profile.displayName,
            provider: 'github'
        };
        var updates = {
            name: profile.displayName,
            someID: profile.id
        };
        var options = {
            upsert: true,
            new: true
        };
        User.findOneAndUpdate(searchQuery, updates, options, function (err, user) {
            if (err) {
                return done(err);
            }
            else {
                return done(null, user);
            }
        });
    }));
}
init();
module.exports = passport;
//# sourceMappingURL=github.js.map