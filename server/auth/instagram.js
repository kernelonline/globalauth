"use strict";
const passport = require("passport");
const User = require("../models/user");
const init = require("./init");
const InstagramStrategy = require('passport-instagram').Strategy;
const config = require("../../config");
if (config.instagram) {
    passport.use(new InstagramStrategy({
        clientID: config.instagram.clientID,
        clientSecret: config.instagram.clientSecret,
        callbackURL: config.instagram.callbackURL,
        profileFields: ['id', 'emails', 'name']
    }, function (accessToken, refreshToken, profile, done) {
        console.log(accessToken, refreshToken, profile);
        var searchQuery = {
            passport_id: profile.id,
            provider: 'instagram'
        };
        var updates = {
            name: '',
            passport_id: profile.id,
            provider: 'instagram',
            email: ''
        };
        var options = {
            upsert: true,
            new: true
        };
        User.findOneAndUpdate(searchQuery, updates, options, function (err, user) {
            if (err) {
                return done(err);
            }
            else {
                return done(null, user);
            }
        });
    }));
}
init();
module.exports = passport;
//# sourceMappingURL=instagram.js.map