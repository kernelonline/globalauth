"use strict";
const passport = require("passport");
const User = require("../models/user");
const init = require("./init");
const localStrategy = require("passport-local");
const LocalStrategy = localStrategy.Strategy;
passport.use(new LocalStrategy({
    usernameField: 'email',
    passwordField: 'passwd'
}, function (email, passwd, done) {
    const searchQuery = {
        provider: 'local',
        email: email,
        password: passwd
    };
    User.find(searchQuery, function (err, user) {
        if (err) {
            console.log("error:" + err);
            return done(err);
        }
        else if (user.length === 1) {
            const u = {};
            for (let i = 0; i < Object.keys(user[0]).length; i++) {
                if (Object.keys(user[0])[i] !== 'password') {
                    u[Object.keys(user[0])[i]] = user[0][Object.keys(user[0])[i]];
                }
            }
            return done(null, u);
        }
        else {
            return done("unauthorized");
        }
    });
}));
init();
module.exports = passport;
//# sourceMappingURL=localLogin.js.map