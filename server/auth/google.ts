import * as passport from "passport"
import { IUser, IUserLogin } from '../../interfaces'
import axios from 'axios'

import * as User from '../models/user'
import * as init from './init'
import { uniqueid } from 'unicoid'
import * as jwt from 'jsonwebtoken'

const GoogleStrategy = require('passport-google-oauth20').Strategy


import * as config from '../../config'
if (config.google) {

  passport.use(new GoogleStrategy({
    clientID: config.google.clientID,
    clientSecret: config.google.clientSecret,
    callbackURL: config.google.callbackURL,
    scope: ['email']
  },
    function (accessToken, refreshToken, profile, done) {


      console.log('PROFILE GOG', profile)
      

      const searchQuery = {
        email: profile.emails[0].value,
        provider: 'google'
      }

      const options = {
        upsert: true,
        new: true
      }



      User.find(searchQuery, function (err, user: any) {
        if (err) {
          console.log(err)
          return done(err)
        } else if (user.length !== 0) {
          console.log(user)
          console.log('login')
          // update the user if s/he exists or add a new user

          const u: IUser = {
            email: profile.emails[0].value,
            passport_id: user[0].passport_id,
            provider: 'google',
            provider_uid: profile._json.id,
            firstName: profile.name.givenName,
            lastName: profile.name.familyName,
            gender: profile.gender,
            avatar: profile.photos[0].value.split('?')[0]+'?sz=512'
          }

          User.findOneAndUpdate(searchQuery, u, options, function (err, user) {
            if (err) {
              return done(err)
            } else {
              return done(null, user)
            }
          })


        } else if (process.env.REGISTER) {
          console.log(user)
          console.log('register')

          const u: IUser = {
            email: profile.emails[0].value,
            passport_id: 'u_' + uniqueid(5) + '_' + Date.now(),
            provider: 'google',
            provider_uid: profile._json.id,
            firstName: profile.name.givenName,
            lastName: profile.name.familyName,
            gender: profile.gender,
            avatar: profile.photos[0].value.split('?')[0]+'?sz=512'
          }

          User.findOneAndUpdate(searchQuery, u, options, function (err, user) {
            if (err) {
              return done(err)
            } else {
              if (process.env.REGISTERHOOK) axios.post(process.env.REGISTERHOOK, u)
              return done(null, user)
            }
          })
        } else {
          return done(new Error('authorization not allowed'))
        }

      })



    }

  ))
}
// serialize user into the session
init()


export = passport
