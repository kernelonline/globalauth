import * as passport from "passport"

import * as User from '../models/user'
import * as init from './init'

const GitHubStrategy = require('passport-github2').Strategy


import * as config from '../../config'
if(config.github){

passport.use(new GitHubStrategy({
  clientID: config.github.clientID,
  clientSecret: config.github.clientSecret,
  callbackURL: config.github.callbackURL
  },
  function(accessToken, refreshToken, profile, done) {

    var searchQuery = {
      name: profile.displayName,
      provider: 'github'
    }

    var updates = {
      name: profile.displayName,
      someID: profile.id
    }

    var options = {
      upsert: true,
        new:true
    }

    // update the user if s/he exists or add a new user
    User.findOneAndUpdate(searchQuery, updates, options, function(err, user) {
      if(err) {
        return done(err)
      } else {
        return done(null, user)
      }
    })
  }

))
}
// serialize user into the session
init()


export = passport
