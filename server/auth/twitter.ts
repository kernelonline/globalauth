import * as passport from "passport"


import * as User from '../models/user'

import * as init from './init'

const TwitterStrategy = require('passport-twitter').Strategy

import * as config from '../../config'


if(config.twitter){

passport.use(new TwitterStrategy({
    consumerKey: config.twitter.consumerKey,
    consumerSecret: config.twitter.consumerSecret,
    callbackURL: config.twitter.callbackURL
  },
  function(accessToken, refreshToken, profile, done) {

    var searchQuery = {
      name: profile.displayName,
      provider: 'twitter'
    }

    var updates = {
      name: profile.displayName,
      someID: profile.id
    }

    var options = {
      upsert: true,
        new:true
    }

    // update the user if s/he exists or add a new user
    User.findOneAndUpdate(searchQuery, updates, options, function(err, user) {
      if(err) {
        return done(err)
      } else {
        return done(null, user)
      }
    })
  }

))
}
// serialize user into the session
init()


export = passport
