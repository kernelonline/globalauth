"use strict";
const passport = require("passport");
const axios_1 = require("axios");
const User = require("../models/user");
const init = require("./init");
const unicoid_1 = require("unicoid");
const GoogleStrategy = require('passport-google-oauth20').Strategy;
const config = require("../../config");
if (config.google) {
    passport.use(new GoogleStrategy({
        clientID: config.google.clientID,
        clientSecret: config.google.clientSecret,
        callbackURL: config.google.callbackURL,
        scope: ['email']
    }, function (accessToken, refreshToken, profile, done) {
        console.log('PROFILE GOG', profile);
        const searchQuery = {
            email: profile.emails[0].value,
            provider: 'google'
        };
        const options = {
            upsert: true,
            new: true
        };
        User.find(searchQuery, function (err, user) {
            if (err) {
                console.log(err);
                return done(err);
            }
            else if (user.length !== 0) {
                console.log(user);
                console.log('login');
                const u = {
                    email: profile.emails[0].value,
                    passport_id: user[0].passport_id,
                    provider: 'google',
                    provider_uid: profile._json.id,
                    firstName: profile.name.givenName,
                    lastName: profile.name.familyName,
                    gender: profile.gender,
                    avatar: profile.photos[0].value.split('?')[0] + '?sz=512'
                };
                User.findOneAndUpdate(searchQuery, u, options, function (err, user) {
                    if (err) {
                        return done(err);
                    }
                    else {
                        return done(null, user);
                    }
                });
            }
            else if (process.env.REGISTER) {
                console.log(user);
                console.log('register');
                const u = {
                    email: profile.emails[0].value,
                    passport_id: 'u_' + unicoid_1.uniqueid(5) + '_' + Date.now(),
                    provider: 'google',
                    provider_uid: profile._json.id,
                    firstName: profile.name.givenName,
                    lastName: profile.name.familyName,
                    gender: profile.gender,
                    avatar: profile.photos[0].value.split('?')[0] + '?sz=512'
                };
                User.findOneAndUpdate(searchQuery, u, options, function (err, user) {
                    if (err) {
                        return done(err);
                    }
                    else {
                        if (process.env.REGISTERHOOK)
                            axios_1.default.post(process.env.REGISTERHOOK, u);
                        return done(null, user);
                    }
                });
            }
            else {
                return done(new Error('authorization not allowed'));
            }
        });
    }));
}
init();
module.exports = passport;
//# sourceMappingURL=google.js.map