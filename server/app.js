"use strict";
const express = require("express");
const cors = require("cors");
const path = require("path");
const bodyParser = require("body-parser");
const Swig = require("swig");
const logger = require("morgan");
const pathExists = require("path-exists");
const passport = require("passport");
const session = require("express-session");
const mongoose = require("mongoose");
const routes = require("./routes/index");
const app = express();
app.use(cors());
let mongohost = "localhost";
let mongoport = "27017";
let mongocoll = "passportbeontvauth";
let mongooseUri;
if (process.env.MONGOURI) {
    mongooseUri = process.env.MONGOURI;
}
else {
    if (process.env.MONGO_HOST)
        mongohost = process.env.MONGO_HOST;
    if (process.env.MONGO_PORT)
        mongoport = process.env.MONGO_PORT;
    if (process.env.MONGO_COLLECTION)
        mongocoll = process.env.MONGO_COLLECTION;
    if (process.env.MONGO_OPTS) {
        mongooseUri = 'mongodb://' + mongohost + ':' + mongoport + '/' + mongocoll + '?' + process.env.MONGO_OPTS;
    }
    else {
        mongooseUri = 'mongodb://' + mongohost + ':' + mongoport + '/' + mongocoll;
    }
}
if (!mongooseUri)
    throw new Error('No mongoose connection provided by env options');
mongoose.connect(mongooseUri);
mongoose.connection.on('disconnected', function () {
    console.error('MongoDB event disconnected');
    process.exit(1);
});
mongoose.connection.on('error', function (err) {
    console.error('MongoDB event error: ' + err);
    process.exit(1);
});
const swig = new Swig.Swig();
app.engine('html', swig.renderFile);
app.set('view engine', 'html');
app.set('views', path.join(__dirname, 'views'));
if (pathExists.sync(__dirname + '/htmlapp')) {
    app.use('/', express.static(path.join(__dirname, 'htmlapp')));
}
let secret = 'keybty44oarrrrat';
if (process.env.secretpw) {
    secret = process.env.secretpw;
}
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, '../client/public')));
app.use(session({
    secret: secret,
    resave: true,
    saveUninitialized: true
}));
app.use(passport.initialize());
app.use(passport.session());
app.use('/', routes);
app.use(function (req, res, next) {
    const err = { message: new Error('Not Found') };
    err.status = 404;
    next(err);
});
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});
module.exports = app;
//# sourceMappingURL=app.js.map