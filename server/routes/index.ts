import { Router } from "express"

import * as _ from 'lodash'

import * as jwt from 'jsonwebtoken'
import axios from 'axios'

import { uniqueid } from 'unicoid'

import { IUser, IUserLogin } from '../../interfaces'


import * as loki from 'lokijs'

const passportTwitter = require('../auth/twitter')
const passportGithub = require('../auth/github')
const passportInstagram = require('../auth/instagram')
const passportFacebook = require('../auth/facebook')
const passportGoogle = require('../auth/google')



import * as User from '../models/user'

import * as config from '../../config'

const router = Router()

const db = new loki('lokidb');
const confirmations = db.addCollection('confirmations')

router.get('/authentication', function (req, res, next) {
  res.render('authentication', { title: 'Express' })
})


router.get('/login', function (req, res, next) {
  res.send('Go back and register!')
})


if (process.env.TWITTER_SECRET) {
  router.get('/auth/twitter', passportTwitter.authenticate('twitter'))

  router.get('/auth/twitter/callback',
    passportTwitter.authenticate('twitter', { failureRedirect: '/login' }),
    function (req, res) {
      // Successful authentication

      // TODO: failure redirect is wrong, correct with right exception


      const u = <IUserLogin>{
        email: req.user.email,
        passport_id: req.user.passport_id
      }
      const token = jwt.sign(u, config.secret)
      u.token = token
      res.render('callback', { title: 'Express', data: u })
    })

}
if (process.env.GITHUB_SECRET) {

  router.get('/auth/github', passportGithub.authenticate('github', { scope: ['user:email'] }))

  router.get('/auth/github/callback',
    passportGithub.authenticate('github', { failureRedirect: '/login' }),
    function (req, res) {
      // Successful authentication

      // TODO: failure redirect is wrong, correct with right exception


      const u = <IUserLogin>{
        email: req.user.email,
        passport_id: req.user.passport_id
      }
      const token = jwt.sign(u, config.secret)
      u.token = token
      res.render('callback', { title: 'Express', data: u })
    })
}
if (process.env.INSTAGRAM_SECRET) {

  router.get('/auth/instagram', passportInstagram.authenticate('instagram', { scope: ['basic', 'likes', 'comments', 'relationships', 'public_content'] }))

  router.get('/auth/instagram/callback',
    passportInstagram.authenticate('instagram', { failureRedirect: '/login' }),
    function (req, res) {
      // Successful authentication

      // TODO: failure redirect is wrong, correct with right exception


      const u = <IUserLogin>{
        email: req.user.email,
        passport_id: req.user.passport_id
      }
      const token = jwt.sign(u, config.secret)
      u.token = token
      res.render('callback', { title: 'Express', data: u })
    })
}


if (process.env.FACEBOOK_SECRET) {

  router.get('/auth/facebook', passportFacebook.authenticate('facebook', { scope: ['email'] }))

  router.get('/auth/facebook/callback',
    passportFacebook.authenticate('facebook', { failureRedirect: '/login' }),
    function (req, res) {
      // Successful authentication

      // TODO: failure redirect is wrong, correct with right exception


      const u = <IUserLogin>{
        email: req.user.email,
        passport_id: req.user.passport_id
      }
      const token = jwt.sign(u, config.secret)



      const useranswer: IUserLogin = {
        token: token,
        email: req.user.email,
        passport_id: req.user.passport_id,
        provider: req.user.provider,
        provider_uid: req.user.provider_uid,
        firstName: req.user.firstName,
        lastName: req.user.lastName,
        gender: req.user.gender,
        avatar: req.user.avatar
      }



      res.render('callback', { title: 'Express', data: useranswer })
    })
}


if (process.env.GOOGLE_SECRET) {

  router.get('/auth/google', passportGoogle.authenticate('google', { scope: ['email'] }))

  router.get('/auth/google/callback',
    passportGoogle.authenticate('google', { scope: ['email'] }),
    function (req, res) {
      // Successful authentication

      // TODO: failure redirect is wrong, correct with right exception


      const u = <IUserLogin>{
        email: req.user.email,
        passport_id: req.user.passport_id
      }
      const token = jwt.sign(u, config.secret)


      const useranswer: IUserLogin = {
        token: token,
        email: req.user.email,
        passport_id: req.user.passport_id,
        provider: req.user.provider,
        provider_uid: req.user.provider_uid,
        firstName: req.user.firstName,
        lastName: req.user.lastName,
        gender: req.user.gender,
        avatar: req.user.avatar
      }


      res.render('callback', { title: 'Express', data: useranswer })
    })
}

if (process.env.LOCALAUTH) {

  router.post('/auth/local/login', (req, res) => {
    const params = req.body

    if (params.email && params.passwd) {

      // TODO: wrong password!!!!

      // update the user if s/he exists or add a new user
      User.find({ email: params.email, provider: 'local' }, function (err, user: any) {
        if (err) {
          console.log("error:" + err)
          res.json({ error: err })
        } else if (user.length === 1) {
          if (user[0].password && user[0].password === params.passwd) {
            const utoken = <IUserLogin>{
              email: user[0].email,
              passport_id: user[0].passport_id
            }
            const token = jwt.sign(utoken, config.secret)

            const useranswer: IUserLogin = {
              token: token,
              email: user[0].email,
              passport_id: user[0].passport_id,
              provider: user[0].provider,
              provider_uid: user[0].provider_uid,
              firstName: user[0].firstName,
              lastName: user[0].lastName,
              gender: user[0].gender,
              avatar: user[0].avatar
            }

            res.json(useranswer)
          } else {
            if (!user[0].password) {
              res.json({ error: "no password associated with this email" })

            } else if (user[0].password !== params.passwd) {
              res.json({ error: "unauthorized!" })

            }
          }

        } else {
          res.json({ error: "unauthorized" })
        }
      })
    } else {
      res.json({ error: 'wrong login params' })
    }
  })

  if (process.env.REGISTER) {

    // TODO: to test, this is only a sketch

    router.post('/auth/local/addpassword', (req, res) => {
      const params = req.body
      if (params.token && params.password) {
        try {
          const token = jwt.verify(params.token, config.secret)
          if (token.passport_id) {

            const options = {
              upsert: true,
              new: true
            }

            User.findOneAndUpdate({ email: token.email, provider: 'local' }, { $set: { password: params.password } }, options, function (err, user: any) {
              if (err) {
                console.log("error:" + err)
                res.json({ error: err })
              } else {
                res.json({ ok: true })
              }
            })


          } else {
            res.json({ error: 'malformed token' })
          }
        } catch (err) {
          res.json({ error: 'unauthorized' })
        }
      } else {
        res.json({ error: 'no token or password provided' })
      }
    })



    router.post('/auth/local/register', (req, res) => {

      const params = req.body

      if (params.email && params.passwd) {

        const passid = 'u_' + uniqueid(5) + '_' + Date.now()

        const newUser: IUser = {
          passport_id: passid,
          email: params.email,
          provider: 'local',
          provider_uid: passid,
          firstName: '',
          lastName: '',
          gender: '',
          avatar: ''
        }
        const newUserAnswer: IUserLogin = JSON.parse((JSON.stringify(newUser)))

        newUser.password = params.passwd

        const token = jwt.sign(newUser, config.secret)

        newUserAnswer.token = token
        // update the user if s/he exists or add a new user
        User.find({ email: params.email }, function (err, user) {

          if (err) {

            res.json({ error: err })

          } else if (user.length === 0) {

            User.create(newUser, function (err, user: any) {
              if (err) {
                res.json({ error: err })
              } else {
                if (process.env.REGISTERHOOK) axios.post(process.env.REGISTERHOOK, newUser)
                res.json(newUserAnswer)
              }
            })

          } else {
            res.json({ error: "just exists" })

          }
        })

      } else {
        res.json({ error: "wrong params" })

      }
    })




  }
}


router.post('/auth/token', (req, res) => {
  const params = req.body
  if (params.token) {
    // verify a token symmetric
    jwt.verify(params.token, config.secret, function (err, decoded) {
      if (err) {
        res.json({ error: 'unauthorized' })
      } else {

        User.find({ passport_id: decoded.passport_id }, function (err, user: IUser[]) {
          if (err) {
            console.log(err)

            res.json({ error: 'not found' })


          } else if (user.length !== 0) {

            const answer: IUser = {
              email: decoded.email,
              passport_id: decoded.passport_id,
              firstName: user[0].firstName,
              lastName: user[0].lastName,
              provider: user[0].provider,
              provider_uid: user[0].provider_uid,
              gender: user[0].gender,
              avatar: user[0].avatar
            }

            res.json(answer)

          }

        })


      }
    })
  } else {
    res.json({ error: 'no token provided' })
  }

})




export = router
