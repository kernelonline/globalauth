import * as mongoose from "mongoose"
const Schema = mongoose.Schema


// create User Schema
const User = new Schema({
  name: String,
  passport_id: String,
  provider: String,
  provider_uid: String,
  email: String,
  password: String,
  token: String,
  updatedAt: Number,
  firstName: String,
  lastName: String,
  gender: String,
  avatar: String
})


const mongousermodel = mongoose.model('users', User)

export = mongousermodel