// *** main dependencies *** //
import * as express from "express"
import * as cors from "cors"

import * as path from "path"

import * as bodyParser from "body-parser"

import * as Swig from "swig"

import * as logger from "morgan"

import * as pathExists from "path-exists"



import * as passport from "passport"
import * as session from "express-session"
import * as mongoose from "mongoose"

// *** routes *** //
import * as routes from "./routes/index"



// *** express instance *** //
const app = express()

app.use(cors())


// *** mongoose *** //

let mongohost = "localhost"
let mongoport = "27017"
let mongocoll = "passportbeontvauth" // DEPRECATED!!!!


let mongooseUri:string

if(process.env.MONGOURI){
  mongooseUri=process.env.MONGOURI
} else {
  if (process.env.MONGO_HOST) mongohost = process.env.MONGO_HOST
  if (process.env.MONGO_PORT) mongoport = process.env.MONGO_PORT
  if (process.env.MONGO_COLLECTION) mongocoll = process.env.MONGO_COLLECTION
  
  if (process.env.MONGO_OPTS){
    mongooseUri='mongodb://' + mongohost + ':' + mongoport + '/'+mongocoll+'?'+process.env.MONGO_OPTS
  } else {
    mongooseUri='mongodb://' + mongohost + ':' + mongoport + '/'+mongocoll
  }
  
}

if (!mongooseUri) throw new Error('No mongoose connection provided by env options')

mongoose.connect(mongooseUri)


mongoose.connection.on('disconnected', function () {
  console.error('MongoDB event disconnected');
  process.exit(1); //evaluate if throw error is better
});

mongoose.connection.on('error', function (err) {
  console.error('MongoDB event error: ' + err);
  process.exit(1); //evaluate if throw error is better
});

// *** view engine *** //
 const swig = new Swig.Swig()



 app.engine('html', swig.renderFile)
 app.set('view engine', 'html')


// *** static directory *** //
app.set('views', path.join(__dirname, 'views'))




if (pathExists.sync(__dirname + '/htmlapp')) {
  app.use('/', express.static(path.join(__dirname, 'htmlapp')))
}


let secret = 'keybty44oarrrrat'

if (process.env.secretpw) {
  secret = process.env.secretpw
}

// *** config middleware *** //
app.use(logger('dev'))

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))

app.use(express.static(path.join(__dirname, '../client/public')))

app.use(session({
  secret: secret,
  resave: true,
  saveUninitialized: true
}))


app.use(passport.initialize())
app.use(passport.session())


// *** main routes *** //
app.use('/', routes)


// catch 404 and forward to error handler
app.use(function (req, res, next) {
  const err: any = { message: new Error('Not Found') }
  err.status = 404
  next(err)
})


// *** error handlers *** //

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function (err, req, res, next) {
    res.status(err.status || 500)
    res.render('error', {
      message: err.message,
      error: err
    })
  })
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
  res.status(err.status || 500)
  res.render('error', {
    message: err.message,
    error: {}
  })
})


export = app
